﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTestAnagram
{
    public class Program
    {
        static void Main(string[] args)
        {

            int iterationCount = 0;

            while (iterationCount != -1)
            {
                Console.WriteLine("Enter Iteration Count (-1 To Exit)");
                iterationCount = Int32.Parse(Console.ReadLine());

                var startTime = DateTime.Now;
                for (int i = 0; i < iterationCount; i++)
                {
                    CheckIsAnagramByDictionary("ala", "laa");
                    CheckIsAnagramByDictionary("alam", "mala");
                    CheckIsAnagramByDictionary("alatas", "tasala");
                    CheckIsAnagramByDictionary("alka", "mala");
                    CheckIsAnagramByDictionary("alkasasba", "balssaaba");
                    CheckIsAnagramByDictionary("asdfasdf", "adsfadsf");
                    CheckIsAnagramByDictionary("sadf", "adsfadsf");
                    CheckIsAnagramByDictionary("sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj", "sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj");
                    CheckIsAnagramByDictionary("asdfasasdfasdf", "fasdfasasdfasd");
                }
                Console.WriteLine("CheckIsAnagramByDictionary result takes " + (DateTime.Now - startTime).TotalMilliseconds + " miliseconds");

                startTime = DateTime.Now;

                for (int i = 0; i < iterationCount; i++)
                {
                    CheckIsAnagramByManipulatingArray("ala", "laa");
                    CheckIsAnagramByManipulatingArray("alam", "mala");
                    CheckIsAnagramByManipulatingArray("alatas", "tasala");
                    CheckIsAnagramByManipulatingArray("alka", "mala");
                    CheckIsAnagramByManipulatingArray("alkasasba", "balssaaba");
                    CheckIsAnagramByManipulatingArray("asdfasdf", "adsfadsf");
                    CheckIsAnagramByManipulatingArray("sadf", "adsfadsf");
                    CheckIsAnagramByManipulatingArray("sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj", "sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj");
                    CheckIsAnagramByManipulatingArray("asdfasasdfasdf", "fasdfasasdfasd");
                }

                Console.WriteLine("CheckIsAnagramByManipulatingArray result takes " + (DateTime.Now - startTime).TotalMilliseconds + " miliseconds");


                startTime = DateTime.Now;

                for (int i = 0; i < iterationCount; i++)
                {
                    CheckIsAnagramByFlaggingUsedItems("ala", "laa");
                    CheckIsAnagramByFlaggingUsedItems("alam", "mala");
                    CheckIsAnagramByFlaggingUsedItems("alatas", "tasala");
                    CheckIsAnagramByFlaggingUsedItems("alka", "mala");
                    CheckIsAnagramByFlaggingUsedItems("alkasasba", "balssaaba");
                    CheckIsAnagramByFlaggingUsedItems("asdfasdf", "adsfadsf");
                    CheckIsAnagramByFlaggingUsedItems("sadf", "adsfadsf");
                    CheckIsAnagramByFlaggingUsedItems("sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj", "sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj");
                    CheckIsAnagramByFlaggingUsedItems("asdfasasdfasdf", "fasdfasasdfasd");
                }

                Console.WriteLine("CheckIsAnagramByFlaggingUsedItems result takes " + (DateTime.Now - startTime).TotalMilliseconds + " miliseconds");
                Console.WriteLine("---------------------------------------------------");

            }

        }

        public static bool CheckIsAnagramByFlaggingUsedItems(string source, string test)
        {
            int sourceLength = source.Length;
            int testLength = test.Length;

            if (sourceLength == testLength)
            {
                bool[] usedItems = new bool[sourceLength];
                int totalMatchCount = 0;
                for (int i = 0; i < testLength; i++)
                {
                    for (int j = 0; j < sourceLength; j++)
                    {
                        if (source[j] != test[i] || usedItems[j]) continue;
                        usedItems[j] = true;
                        totalMatchCount++;
                        break;
                    }
                }

                return (totalMatchCount == sourceLength);
            }

            return false;
        }

        public static bool CheckIsAnagramByManipulatingArray(string source, string test)
        {
            int sourceLength = source.Length;
            int testLength = test.Length;
            char[] sourceArray = source.ToCharArray();

            if (sourceLength == testLength)
            {
                for (int i = 0; i < testLength; i++)
                {
                    int charIndex = Array.FindIndex(sourceArray, c => c == test[i]);
                    if (charIndex == -1)
                    {
                        return false;
                    }
                    sourceArray[charIndex] = '#';
                }
                return true;
            }
            return false;
        }

        public static bool CheckIsAnagramByDictionary(string source, string test)
        {
            int sourceLength = source.Length;
            int testLength = test.Length;
            Dictionary<char, int> charCountList = new Dictionary<char, int>(testLength);
            if (sourceLength == testLength)
            {
                for (int i = 0; i < sourceLength; i++)
                {
                    char sourceChar = source[i];

                    int value;

                    if (charCountList.TryGetValue(sourceChar, out value))
                    {
                        charCountList[sourceChar] = value + 1;
                    }
                    else
                    {
                        charCountList.Add(sourceChar, 1);
                    }
                }

                for (int j = 0; j < testLength; j++)
                {
                    int value;
                    char key = test[j];

                    if (charCountList.TryGetValue(key, out value))
                    {
                        if (value > 0)
                            charCountList[key]--;
                        else
                            return false;
                    }
                    else
                        return false;
                }

                return true;
            }

            return false;
        }
    }
}