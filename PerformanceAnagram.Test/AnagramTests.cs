﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PerformanceTestAnagram;

namespace PerformanceAnagram.Test
{
    [TestClass]
    public class AnagramTests
    {
        [TestMethod]
        public void CheckIsAnagramByFlaggingUsedItems()
        {
            Assert.IsTrue(Program.CheckIsAnagramByFlaggingUsedItems("ala", "laa"));
            Assert.IsTrue(Program.CheckIsAnagramByFlaggingUsedItems("alam", "mala"));
            Assert.IsTrue(Program.CheckIsAnagramByFlaggingUsedItems("alatas", "tasala"));
            Assert.IsFalse(Program.CheckIsAnagramByFlaggingUsedItems("alka", "mala"));
            Assert.IsFalse(Program.CheckIsAnagramByFlaggingUsedItems("alkasasba", "balssaaba"));
            Assert.IsFalse(Program.CheckIsAnagramByFlaggingUsedItems("sadf", "adsfadsf"));
            Assert.IsTrue(Program.CheckIsAnagramByFlaggingUsedItems("asdfasdf", "adsfadsf"));
            Assert.IsTrue(Program.CheckIsAnagramByFlaggingUsedItems("sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj", "jsadfasdfikljasdfilkjasdfilkjasdfilkjasdilk"));
        }

        [TestMethod]
        public void CheckIsAnagramByManipulatingArray()
        {
            Assert.IsTrue(Program.CheckIsAnagramByManipulatingArray("ala", "laa"));
            Assert.IsTrue(Program.CheckIsAnagramByManipulatingArray("alam", "mala"));
            Assert.IsTrue(Program.CheckIsAnagramByManipulatingArray("alatas", "tasala"));
            Assert.IsFalse(Program.CheckIsAnagramByManipulatingArray("alka", "mala"));
            Assert.IsFalse(Program.CheckIsAnagramByManipulatingArray("alkasasba", "balssaaba"));
            Assert.IsFalse(Program.CheckIsAnagramByManipulatingArray("sadf", "adsfadsf"));
            Assert.IsTrue(Program.CheckIsAnagramByManipulatingArray("asdfasdf", "adsfadsf"));
            Assert.IsTrue(Program.CheckIsAnagramByManipulatingArray("sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj", "jsadfasdfikljasdfilkjasdfilkjasdfilkjasdilk"));
        }

        [TestMethod]
        public void CheckIsAnagramByDictionary()
        {
            Assert.IsTrue(Program.CheckIsAnagramByDictionary("ala", "laa"));
            Assert.IsTrue(Program.CheckIsAnagramByDictionary("alam", "mala"));
            Assert.IsTrue(Program.CheckIsAnagramByDictionary("alatas", "tasala"));
            Assert.IsFalse(Program.CheckIsAnagramByDictionary("alka", "mala"));
            Assert.IsFalse(Program.CheckIsAnagramByDictionary("alkasasba", "balssaaba"));
            Assert.IsFalse(Program.CheckIsAnagramByDictionary("sadf", "adsfadsf"));
            Assert.IsTrue(Program.CheckIsAnagramByDictionary("asdfasdf", "adsfadsf"));
            Assert.IsTrue(Program.CheckIsAnagramByDictionary("sadfasdfikljasdfilkjasdfilkjasdfilkjasdilkj", "jsadfasdfikljasdfilkjasdfilkjasdfilkjasdilk"));
        }
    }
}
